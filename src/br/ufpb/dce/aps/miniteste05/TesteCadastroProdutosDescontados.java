package br.ufpb.dce.aps.miniteste05;

import org.junit.*;

public class TesteCadastroProdutosDescontados {

	@Test
	public void testAddProdutos() {
		GerenciadorDeProdutos gerente = new GerenciadorDeProdutos();
		gerente.add(new Produto(12.00,new CalculadorDeDescontoPadrao()));
		gerente.get(0).setPreco();
		Assert.assertEquals(10.80, gerente.get(0).getPreco(),0.0);
		
		gerente.add(new Produto(12.00,new CalculadorDeDescontoEspecial()));
		gerente.get(1).setPreco();
		Assert.assertEquals(08.40, gerente.get(1).getPreco(),0.0);	
	}

}
