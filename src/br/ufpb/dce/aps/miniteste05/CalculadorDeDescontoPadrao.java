package br.ufpb.dce.aps.miniteste05;

public class CalculadorDeDescontoPadrao implements ICalculadorDeDesconto {
	
	public final double DESCONTO_PADRAO = 0.1;

	@Override
	public double calcularDescontoPara(Produto produto) {
		return produto.getPreco() * DESCONTO_PADRAO;
	}

}
