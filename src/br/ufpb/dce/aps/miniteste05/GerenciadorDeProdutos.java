package br.ufpb.dce.aps.miniteste05;

import java.util.ArrayList;
import java.util.List;

public class GerenciadorDeProdutos {
	
	List<Produto> produtos;
	
	public GerenciadorDeProdutos(){
		this.produtos = new ArrayList<Produto>();
	}
	
	public void add(Produto produto){
		produtos.add(produto);
	}
	public Produto get(int posicao){
		return produtos.get(posicao);
		
	}

}
