package br.ufpb.dce.aps.miniteste05;

public class Produto{
	
	private double preco;
	private ICalculadorDeDesconto desconto;
	
	public Produto(double preco, ICalculadorDeDesconto desconto){
		this.preco = preco;
		this.desconto = desconto;
	}
	
	public double getPreco(){
		return this.preco;
	}
	
	public void setPreco(){
		this.preco -= calcularDesconto(); 
	}
	
	public double calcularDesconto(){
		return this.desconto.calcularDescontoPara(this);
	}
	
}
