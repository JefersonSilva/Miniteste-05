package br.ufpb.dce.aps.miniteste05;

public class CalculadorDeDescontoEspecial implements ICalculadorDeDesconto {
	
	public final double DESCONTO_ESPECIAL = 0.3;

	@Override
	public double calcularDescontoPara(Produto produto) {		
		return produto.getPreco() * DESCONTO_ESPECIAL;
	}

}
