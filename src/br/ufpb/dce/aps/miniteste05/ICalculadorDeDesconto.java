package br.ufpb.dce.aps.miniteste05;

public interface ICalculadorDeDesconto {
	
	double calcularDescontoPara(Produto produto);

}
